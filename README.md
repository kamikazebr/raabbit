# Raabbit Android app #

The Raabbit app is commitment's alarm, using [Couch Sync Gateway](https://github.com/couchbase/sync_gateway) althought [RaabbitServer](https://bitbucket.org/kamikazebr/raabbitserver/) to store and replicate on sync by account the commitments saved.

[Couchbase](https://github.com/couchbase) is NoSQL database with sync built-in.
[RaabbitServer](https://bitbucket.org/kamikazebr/raabbitserver/) using Node.js small and fast RestFul API for login and create user.

## Features

* Login with Facebook account or Raabbit account created on app.
* Create commitment with alarm and sound choice.
* Sync all commitments beetween devices connected in same account.
* Search by tittle commitmment.
* Support >= API 15 (Android 4.0.3 Ice Cream Sandwich)

## How do I get set up?

* Clone the repo and build with gradle or Android Studio

## Improvements

* Reminder timer
* Commitment's description
* Javadoc
* Mockito on Unit Test

## Who do I talk to?

* Repo owner Felipe Novaes F. da Rocha (Phillip Kamikaze) [windholyghost@gmail.com](mailto://windholyghost@gmail.com)

## License
	Copyright 2016 Felipe Novaes F. da Rocha

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

   		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
package br.com.kamikaze.o2b.raabbit;

import android.util.Log;

import br.com.kamikaze.o2b.raabbit.model.User;
import br.com.kamikaze.o2b.raabbit.util.RestManager;
import retrofit2.Response;

/**
 * Created by Felipe Novaes F. da Rocha (Kamikaze) on 11/06/16.
 */
//@RunWith(AndroidJUnit4.class)
public class RestTest {

//    @Test
    public void user(){
//        System.out.println("Oi");
        Response<User> commitmentResponse = RestManager.getInstance().testUserService();
        Log.d(RestTest.class.getSimpleName(),commitmentResponse.body().toString());
    }
}

package br.com.kamikaze.o2b.raabbit.activities;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bignerdranch.android.multiselector.ModalMultiSelectorCallback;
import com.bignerdranch.android.multiselector.MultiSelector;
import com.bignerdranch.android.multiselector.SwappingHolder;
import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Document;
import com.couchbase.lite.LiveQuery;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryEnumerator;
import com.couchbase.lite.QueryRow;
import com.couchbase.lite.replicator.Replication;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import br.com.kamikaze.o2b.raabbit.R;
import br.com.kamikaze.o2b.raabbit.RaabbitApplication;
import br.com.kamikaze.o2b.raabbit.model.Commitment;
import br.com.kamikaze.o2b.raabbit.model.User;
import br.com.kamikaze.o2b.raabbit.util.PreferencesManager;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {


    //    public static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    public static final int REQ_CODE_CREATE_COMMITMENT = 111;
    private static final String TAG = MainActivity.class.getSimpleName();

    private MyAdapter myAdapter;
    private PreferencesManager preferencesManager;
    private List<Commitment> commitmentList;
    @InjectView(R.id.list_commitments)
    protected RecyclerView list;

    @InjectView(R.id.toolbar)
    protected Toolbar toolbar;
    protected TextView txtNavName;
    protected TextView txtNavEmail;
    RaabbitApplication application;
    private Query query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        setSupportActionBar(toolbar);

        application = (RaabbitApplication) getApplication();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerLayout = navigationView.getHeaderView(0);
        txtNavName = (TextView) headerLayout.findViewById(R.id.txtNavName);
        txtNavEmail = (TextView) headerLayout.findViewById(R.id.txtNavEmail);

        preferencesManager = new PreferencesManager(getApplication());

        if (!preferencesManager.isLogged()) {
            startActivity(new Intent(this, LoginChooserActivity.class));
            finish();
        } else {
            application.getOnSyncProgressChangeObservable().addObserver(new Observer() {
                @Override
                public void update(Observable observable, final Object data) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            RaabbitApplication.SyncProgress progress = (RaabbitApplication.SyncProgress) data;
                            Snackbar snackbar = Snackbar.make(MainActivity.this.toolbar, String.format("Sincronizando %d/%d - %s", progress.completedCount, progress.totalCount, progress.status), Snackbar.LENGTH_SHORT);

                            if (progress.status == Replication.ReplicationStatus.REPLICATION_ACTIVE) {
                                if (progress.totalCount > 0 && progress.completedCount == progress.totalCount) {
//                                    Notifications.getInstance().show(MainActivity.this,"Sincronização",progress.totalCount+" compromissos foram sincronizados",0);
                                }
                                com.couchbase.lite.util.Log.d(TAG, "Turn on progress spinny");
                                setProgressBarIndeterminateVisibility(true);
                                snackbar.show();
                            } else {
                                com.couchbase.lite.util.Log.d(TAG, "Turn off progress spinny");
                                setProgressBarIndeterminateVisibility(false);
                                snackbar.dismiss();
                            }
                        }
                    });
                }
            });
            txtNavEmail.setText(preferencesManager.getCurrentUserEmail());
            final String emailOrUserID = preferencesManager.getCurrentUserIDOrEmail();
            Query queryByEmailOrUserID = User.getQueryByEmailOrUserID(application.getMainDatabase(), emailOrUserID);
            queryByEmailOrUserID.runAsync(new Query.QueryCompleteListener() {
                @Override
                public void completed(QueryEnumerator rows, Throwable error) {
                    if (rows.getCount() > 0) {
                        QueryRow row = rows.getRow(0);
                        final User user = User.fromDocument(row.getDocument());
                        if (user!=null) {
                            String password = user.getPassword();
                            application.setDatabaseForName(emailOrUserID);
                            query = Commitment.getQuery(application.getDatabase());
                            LiveQuery liveQuery = query.toLiveQuery();
                            myAdapter = new MyAdapter(liveQuery);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    txtNavName.setText(user.getName());
                                    list.setAdapter(myAdapter);
                                }
                            });
                            if (password != null) {
                                application.startReplicationSyncWithBasicAuth(user.getEmail(), password);
                            } else {
                                application.startReplicationSyncWithFacebookLogin(preferencesManager.getFbAccessToken());
                            }
                            runQuery();
                        }
                    }
                }
            });

            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            list.setLayoutManager(layoutManager);
            list.setHasFixedSize(true);


        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("MainActivity.onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("MainActivity.onResume");
        runQuery();
    }

    private void runQuery() {
        if (query!=null) {
            query.runAsync(new Query.QueryCompleteListener() {
                @Override
                public void completed(final QueryEnumerator rows, Throwable error) {
                    System.out.println("MainActivity.completed");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            myAdapter.setEnumerator(rows);
                            commitmentList = myAdapter.getListCommitments();
                        }
                    });
                }
            });
        }
    }


    protected void listCommitmentsClick(View view) {
        System.out.println(view);
        startCreateCommitment(null);
    }

    @OnClick(R.id.fab)
    protected void fabClick() {
        //TODO createcommitmentActivity
        startCreateCommitment(null);
    }

    private void startCreateCommitment(Commitment commitment) {
        Intent intent = new Intent(MainActivity.this, CreateCommitmentActivity.class);
        if (commitment != null) {
            intent.putExtra(CreateCommitmentActivity.COMMITMENT_BUNDLE, commitment);
        }
        startActivityForResult(intent, REQ_CODE_CREATE_COMMITMENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE_CREATE_COMMITMENT) {
            if (data != null) {
                Log.d(TAG, "Extras:" + data.getExtras());
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        searchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(this, MainActivity.class)));

        searchView.setQueryHint(getString(R.string.search_view_hint));

        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_search) {
            return true;
        }else if (id == R.id.menu_create){
            fabClick();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        if (id == R.id.nav_logout) {
            application.logoutUser();
            Intent intent = new Intent(this, LoginChooserActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        myAdapter.filter(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        myAdapter.filter(newText);
        return true;

    }

    private MultiSelector mMultiSelector = new MultiSelector();

    private ActionMode actionModeDelete;
    private ModalMultiSelectorCallback mActionModeCallback
            = new ModalMultiSelectorCallback(mMultiSelector) {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            super.onCreateActionMode(mode, menu);
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.action_mode_list, menu);
            mode.setTitle(getString(R.string.title_delete_mode));
            mode.setSubtitle(getString(R.string.subtitle_delete_mode_1));
            actionModeDelete = mode;
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            super.onDestroyActionMode(actionMode);
            actionModeDelete = null;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            //TODO Show one dialog ask for confirmation about delete
            if (item.getItemId() == R.id.nav_delete) {
                mode.finish();
                QueryEnumerator enumerator = myAdapter.getEnumerator();

                for (int i = enumerator.getCount(); i >= 0; i--) {
                    if (mMultiSelector.isSelected(i, 0)) {
                        QueryRow next = enumerator.getRow(i);
                        String documentId = next.getDocumentId();
                        System.out.println(documentId);
                        Document document = application.getDatabase().getExistingDocument(documentId);
                        if (document != null) {

                            Commitment commitment = new Commitment(document);
                            commitment.updateEnableStatus(application, false);
                            try {
                                System.out.println(document.getProperties());
                                document.delete();
                            } catch (CouchbaseLiteException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                mMultiSelector.clearSelections();
                return true;
            }
            return false;
        }
    };

    public class CommitmentViewHolder extends SwappingHolder implements View.OnLongClickListener, View.OnClickListener, CompoundButton.OnCheckedChangeListener {
        @InjectView(R.id.txtDateAlarm)
        public TextView txtDateAlarm;
        @InjectView(R.id.txtDateLimit)
        public TextView txtDateLimit;
        @InjectView(R.id.txtTitle)
        public TextView txtTitle;
        @InjectView(R.id.toggleEnable)
        protected SwitchCompat toggleEnabled;

        private Commitment commitment;


        public CommitmentViewHolder(View v) {
            super(v, mMultiSelector);
            ButterKnife.inject(this, v);
            v.setOnClickListener(this);
            v.setOnLongClickListener(this);
            toggleEnabled.setOnCheckedChangeListener(this);
        }

        public Commitment getCommitment() {
            return commitment;
        }

        public void setCommitment(Commitment commitment) {
            this.commitment = commitment;
            txtTitle.setText(commitment.getTitle());
            DateTime dateLimit = commitment.getDateLimit();
            String datePretty = null;
            String format = null;
            if (dateLimit != null) {
                datePretty = Commitment.getDatePretty(dateLimit);
                format = dateLimit.toString(Commitment.DD_MM_YYYY_HH_MM);
            }
            txtDateAlarm.setText(datePretty);
            txtDateLimit.setText(format);
            Map<String, Object> existingLocalDocument = application.getMainDatabase().getExistingLocalDocument(commitment.getId());
            boolean isChecked = false;
            if (existingLocalDocument!=null){
                Object enabled = existingLocalDocument.get("enabled");
                if (enabled!=null && !commitment.isPast()){
                    isChecked = (boolean) enabled;
                }
            }
            toggleEnabled.setOnCheckedChangeListener(null);
            toggleEnabled.setChecked(isChecked);
            toggleEnabled.setOnCheckedChangeListener(this);
            commitment.updateEnableStatus(application,isChecked);
        }

        @Override
        public boolean onLongClick(View v) {
            if (!mMultiSelector.isSelectable()) {
                MainActivity.this.startSupportActionMode(mActionModeCallback);

                mMultiSelector.setSelected(CommitmentViewHolder.this, true);
                return true;
            }
            return false;
        }

        @Override
        public void onClick(View v) {
            if (mMultiSelector.isSelectable()) {
                mMultiSelector.setSelected(CommitmentViewHolder.this, !mMultiSelector.isSelected(getAdapterPosition(), getItemId()));
            } else {
                MainActivity.this.startCreateCommitment(this.commitment);
            }
            int countSelected = mMultiSelector.getSelectedPositions().size();
            if (actionModeDelete != null) {
                String formatSelection = "%0$d selecionado";
                if (countSelected == 0) {
                    formatSelection = "Nenhum selecionado";
                } else if (countSelected > 1) {
                    formatSelection += "s";
                }
                actionModeDelete.setSubtitle(String.format(formatSelection, countSelected));
            }
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            boolean saveChangeOnDB = true;
            if (isChecked){
                if (commitment.isPast()){
                    saveChangeOnDB = false;
                    toggleEnabled.setChecked(false);
                    Toast.makeText(MainActivity.this, R.string.toast_change_date_before_enable, Toast.LENGTH_SHORT).show();
                }
            }
            if (saveChangeOnDB) {

                commitment.updateEnableStatus(application,isChecked);
            }
        }

    }


    /////////////////////////***********************/////////////////////
    public interface OnClickAdapterListener {
        void onCommitmentClick(int position, CommitmentViewHolder v);
//        void onCommitmentLongClick(int position, CommitmentViewHolder v);
    }

    public class MyAdapter extends RecyclerView.Adapter<CommitmentViewHolder> {


        private final LiveQuery query;
        private OnClickAdapterListener clickAdapterListener;
        private List<Commitment> mListCommitmentsBackup;
        private List<Commitment> mListCommitments;
        private QueryEnumerator enumerator;


        public MyAdapter(LiveQuery query) {

            this.query = query;

            query.addChangeListener(new LiveQuery.ChangeListener() {
                @Override
                public void changed(final LiveQuery.ChangeEvent event) {
                    ((AppCompatActivity) MainActivity.this).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setEnumerator(event.getRows());
                        }
                    });
                }
            });
            query.start();
        }

        public void setEnumerator(QueryEnumerator enumerator) {
            this.enumerator = enumerator;
            List<Commitment> newList = new ArrayList<Commitment>();
            for (QueryRow queryRow : enumerator) {
                Commitment commitment = new Commitment(queryRow.getDocument());
                if (!newList.contains(commitment)) {
                    newList.add(commitment);
                }
            }
            setListCommitments(newList);
        }

        public List<Commitment> getListCommitments() {
            return mListCommitments;
        }

        public void setListCommitments(List<Commitment> mListCommitments) {
            this.mListCommitments = mListCommitments;
            this.mListCommitmentsBackup = new ArrayList<Commitment>(mListCommitments);
            notifyDataSetChanged();
        }

        public void filter(String query) {

            query = query.toLowerCase().trim();

            final List<Commitment> filteredModelList = new ArrayList<Commitment>();
            if (TextUtils.isEmpty(query)) {
                filteredModelList.addAll(mListCommitmentsBackup);
            } else {
                for (Commitment model : mListCommitmentsBackup) {
                    final String text = model.getTitle().toLowerCase();
                    if (text.contains(query)) {
                        filteredModelList.add(model);
                    }
                }
            }
            this.animateTo(filteredModelList);
            list.scrollToPosition(0);
        }


        public void resetFilter() {
            setListCommitments(mListCommitmentsBackup);
        }

        @Override
        public CommitmentViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {

            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.commitment_list_view, parent, false);

            CommitmentViewHolder vh = new CommitmentViewHolder(v);

            return vh;
        }

        @Override
        public void onBindViewHolder(CommitmentViewHolder holder, int position) {
            holder.setCommitment(getItem(position));
        }

        @Override
        public int getItemCount() {
            return (mListCommitments == null) ? 0 : mListCommitments.size();
        }


        public QueryEnumerator getEnumerator() {
            return enumerator;
        }

        public Commitment getItem(int i) {
            return mListCommitments.get(i);
        }
//        @Override
//        public int getItemCount() {
//            return mListCommitments.size();
//        }

        public OnClickAdapterListener getClickAdapterListener() {
            return clickAdapterListener;
        }

        public void setClickAdapterListener(OnClickAdapterListener clickAdapterListener) {
            this.clickAdapterListener = clickAdapterListener;
        }

        public Commitment removeItem(int position) {
            final Commitment model = mListCommitments.remove(position);
            notifyItemRemoved(position);
            return model;
        }

        public void addItem(int position, Commitment model) {
            mListCommitments.add(position, model);
            notifyItemInserted(position);
        }

        public void moveItem(int fromPosition, int toPosition) {
            final Commitment model = mListCommitments.remove(fromPosition);
            mListCommitments.add(toPosition, model);
            notifyItemMoved(fromPosition, toPosition);
        }

        public void animateTo(List<Commitment> models) {
            applyAndAnimateRemovals(models);
            applyAndAnimateAdditions(models);
            applyAndAnimateMovedItems(models);
        }

        private void applyAndAnimateRemovals(List<Commitment> newModels) {
            for (int i = mListCommitments.size() - 1; i >= 0; i--) {
                final Commitment model = mListCommitments.get(i);
                if (!newModels.contains(model)) {
                    removeItem(i);
                }
            }
        }

        private void applyAndAnimateAdditions(List<Commitment> newModels) {
            for (int i = 0, count = newModels.size(); i < count; i++) {
                final Commitment model = newModels.get(i);
                if (!mListCommitments.contains(model)) {
                    addItem(i, model);
                }
            }
        }

        private void applyAndAnimateMovedItems(List<Commitment> newModels) {
            for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
                final Commitment model = newModels.get(toPosition);
                final int fromPosition = mListCommitments.indexOf(model);
                if (fromPosition >= 0 && fromPosition != toPosition) {
                    moveItem(fromPosition, toPosition);
                }
            }
        }


    }

}

package br.com.kamikaze.o2b.raabbit.util;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import java.util.LinkedHashMap;
import java.util.Map;

import br.com.kamikaze.o2b.raabbit.R;
import br.com.kamikaze.o2b.raabbit.activities.MainActivity;
import br.com.kamikaze.o2b.raabbit.receiver.AlarmNotificationReceiver;

/**
 * Created by Felipe Novaes F. da Rocha (Kamikaze) on 14/06/16.
 */
public class Notifications {

    private static final String GROUP_KEY_ALARMS = "group_alarms";
    public static String NOTIFICATION_ID = "notification_id";
    public static String NOTIFICATION = "notification";

    private static Notifications ourInstance = new Notifications();

    public static Notifications getInstance() {
        return ourInstance;
    }

    private Notifications() {
    }


    public void show(Context context, CharSequence title, CharSequence text, long futureInMillis) {

        Notification notification = getNotification(context, title, text, null);

        Intent notificationIntent = new Intent(context, AlarmNotificationReceiver.class);
        notificationIntent.putExtra(NOTIFICATION_ID, 1);
        notificationIntent.putExtra(NOTIFICATION, notification);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, futureInMillis, pendingIntent);

    }

    public Notification getNotification(Context context, CharSequence title, CharSequence text, Uri soundUri) {
        if (soundUri == null) {
            soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        }
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 333, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.logo_raabbit)
                .setContentTitle(title)
                .setSound(soundUri)
                .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
//                .setGroup(GROUP_KEY_ALARMS)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentText(text).build();
    }

    public void notity(Context context, Notification notification, int id) {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(id, notification);
    }

    public Map<String, Uri> getRingtones(Context context) {
        Map<String, Uri> ringtones = new LinkedHashMap<String, Uri>();

        RingtoneManager ringtoneMgr = new RingtoneManager(context);
        ringtoneMgr.setType(RingtoneManager.TYPE_ALL);
        Cursor alarmsCursor = ringtoneMgr.getCursor();
        while (alarmsCursor.moveToNext()) {
            String id = alarmsCursor.getString(RingtoneManager.ID_COLUMN_INDEX);
            String uri = alarmsCursor.getString(RingtoneManager.URI_COLUMN_INDEX);
            String name = alarmsCursor.getString(RingtoneManager.TITLE_COLUMN_INDEX);
            ringtones.put(name, Uri.parse(uri + "/" + id));
        }
        return ringtones;
    }
}

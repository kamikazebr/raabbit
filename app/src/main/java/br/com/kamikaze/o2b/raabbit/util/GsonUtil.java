package br.com.kamikaze.o2b.raabbit.util;

import android.net.Uri;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.text.DateFormat;

/**
 * Created by Felipe Novaes F. da Rocha (Kamikaze) on 11/06/16.
 */
public class GsonUtil {


    Gson gson = new GsonBuilder().registerTypeAdapter(Uri.class,new UriTypeHierarchyAdapter()).setDateFormat(DateFormat.MEDIUM).create();

    private static GsonUtil ourInstance = new GsonUtil();

    public static GsonUtil getInstance() {
        return ourInstance;
    }

    private GsonUtil() {
    }

    public Gson getGson() {
        return gson;
    }

    public void setGson(Gson gson) {
        this.gson = gson;
    }


    public class UriTypeHierarchyAdapter implements JsonDeserializer<Uri>, JsonSerializer<Uri> {
        @Override
        public Uri deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return Uri.parse(json.getAsString());
        }

        @Override
        public JsonElement serialize(Uri src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(src.toString());
        }
    }
}

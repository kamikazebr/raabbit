package br.com.kamikaze.o2b.raabbit.model;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.Emitter;
import com.couchbase.lite.Mapper;
import com.couchbase.lite.Query;
import com.couchbase.lite.View;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import br.com.kamikaze.o2b.raabbit.util.GsonUtil;

/**
 * Created by Felipe Novaes F. da Rocha (Kamikaze) on 11/06/16.
 */
public class User extends BaseModel{
    private static final String BY_EMAIL_VIEW_NAME = "users_by_email";
    private static final String VIEW_NAME = "users";
    private static final String DOC_TYPE = "user";
    private String _id;
    private String userID;
    private String name;
    private String email;
    private String password;
    private String type= DOC_TYPE;


    public User(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public User() {
    }

    public static User fromDocument(Document document){
        User user = null;
        if (document != null) {
            Map<String, Object> properties = document.getProperties();
            Gson gson = GsonUtil.getInstance().getGson();
            String json = gson.toJson(properties);
            user = gson.fromJson(json, User.class);
        }
        return user;
    }
    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static Query getQueryByEmailOrUserID(Database database, String email) {
        View view = database.getView(BY_EMAIL_VIEW_NAME);
        if (view.getMap() == null) {
            Mapper map = new Mapper() {
                @Override
                public void map(Map<String, Object> document, Emitter emitter) {
                    if (DOC_TYPE.equals(document.get("type"))) {
                        emitter.emit(document.get("email"), document);
                        emitter.emit(document.get("userID"), document);
                    }
                }
            };
            view.setMap(map, "1");
        }

        Query query = view.createQuery();
        java.util.List<Object> keys = new ArrayList<Object>();
        keys.add(email);
        query.setKeys(keys);

        return query;
    }

    public void createOrUpdate(Database database) throws CouchbaseLiteException {
        Document document = null;
        if (userID!=null) {
            document = database.getDocument(userID);
        }else{
            document = database.getDocument(email);
        }
        Map<String, Object> properties = document.getProperties();
        if (properties== null){
            properties = new HashMap<String,Object>();
        }else{
            properties = new HashMap<String,Object>(properties);
        }
        Map<String, Object> stringObjectMap = toJSONMap();
        for (String key : stringObjectMap.keySet()) {
            properties.put(key,stringObjectMap.get(key));
        }

        document.putProperties(properties);
    }
}

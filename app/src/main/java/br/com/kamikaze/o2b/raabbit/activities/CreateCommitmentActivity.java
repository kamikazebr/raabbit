package br.com.kamikaze.o2b.raabbit.activities;

import android.app.Activity;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;
import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Document;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.joda.time.DateTime;

import java.util.Calendar;

import br.com.kamikaze.o2b.raabbit.R;
import br.com.kamikaze.o2b.raabbit.RaabbitApplication;
import br.com.kamikaze.o2b.raabbit.model.Commitment;
import br.com.kamikaze.o2b.raabbit.util.PreferencesManager;
import butterknife.InjectView;
import butterknife.OnClick;

public class CreateCommitmentActivity extends BaseToolbarActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private static final String TIMEPICKER_DIALOG = "TimepickerDialog";
    private static final String DATEPICKERDIALOG = "Datepickerdialog";
    private static final String DATE_FORMAT = "%02d/%02d/%d";
    private static final String TIME_FORMAT = "%02d:%02d";

    public static final String COMMITMENT_BUNDLE = "COMMITMENT_BUNDLE";
    public static final int REQUEST_CODE_RINGTONE = 5;
    @InjectView(R.id.txtTitle)
    protected FormEditText txtTitle;
    @InjectView(R.id.txtDate)
    protected FormEditText txtDate;
    @InjectView(R.id.txtTime)
    protected FormEditText txtTime;

    @InjectView(R.id.txtRing)
    protected EditText txtRing;


    private RaabbitApplication application;

    private Commitment commitment;
    private PreferencesManager pref;
    private Uri uriRingTone;
    private String ringtoneTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_commitment);

        application = (RaabbitApplication) getApplication();
        pref = new PreferencesManager(getApplicationContext());
        Intent intent = getIntent();
        if (intent.hasExtra(COMMITMENT_BUNDLE)) {
            commitment = (Commitment) intent.getSerializableExtra(COMMITMENT_BUNDLE);
            uriRingTone = commitment.getRingtoneUri();

            txtRing.setText(commitment.getRingtoneTitle());
            txtTitle.setText(commitment.getTitle());
            DateTime dateLimit = commitment.getDateLimit();

            if (dateLimit != null) {
                txtDate.setText(dateLimit.toString(Commitment.DD_MM_YYYY));
                txtTime.setText(dateLimit.toString(Commitment.HH_MM));
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        DatePickerDialog dpd = (DatePickerDialog) getFragmentManager().findFragmentByTag(DATEPICKERDIALOG);
        TimePickerDialog tpd = (TimePickerDialog) getFragmentManager().findFragmentByTag(TIMEPICKER_DIALOG);

        if (tpd != null) tpd.setOnTimeSetListener(this);
        if (dpd != null) dpd.setOnDateSetListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_save) {
            System.out.println("CreateCommitmentActivity.onOptionsItemSelected");
            onSave(new CallBack() {

                @Override
                public void done() {
                    setResult(MainActivity.REQ_CODE_CREATE_COMMITMENT);
                    finish();
                }

                @Override
                public void error() {

                }
            });
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public interface CallBack {
        void done();

        void error();
    }

    private void onSave(final CallBack callBack) {
        if (txtTitle.testValidity() && txtDate.testValidity() && txtTime.testValidity()) {

            String dateString = txtDate.getText().toString();
            String timeString = txtTime.getText().toString();
            final DateTime date = DateTime.parse(dateString + " " + timeString, Commitment.formatter);

            if (date.getMillis() <= System.currentTimeMillis()) {
                Toast.makeText(this, R.string.toast_hour_need_be_in_future, Toast.LENGTH_SHORT).show();
            } else {
                final String title = txtTitle.getText().toString();
                if (commitment == null) {
                    commitment = new Commitment();
                }
                String ringToneString = null;
                if (uriRingTone != null) {
                    ringToneString = uriRingTone.toString();
                }
                commitment.setRingtone(ringToneString);
                commitment.setRingtoneTitle(ringtoneTitle);
                commitment.setTitle(title);
                commitment.setDateLimit(date);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Document doc = commitment.createOrUpdateCommitment(application, pref.getCurrentUserIDOrEmail());
                            if (callBack != null) {
                                callBack.done();
                            }
                        } catch (CouchbaseLiteException e) {
                            e.printStackTrace();
                            if (callBack != null) {
                                callBack.error();
                            }
                        }
                    }
                }).start();
            }
        }
    }

    @OnClick({R.id.txtDate, R.id.txtTime})
    protected void onClickTexts(View view) {
        if (view.getId() == R.id.txtDate) {
            openDatePicker();
        } else {
            openTimerPicker();
        }
    }

    private void openDatePicker() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                CreateCommitmentActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setMinDate(now);
        dpd.dismissOnPause(true);
        dpd.show(getFragmentManager(), DATEPICKERDIALOG);
    }


    private void openTimerPicker() {
        Calendar now = Calendar.getInstance();
        TimePickerDialog dpd = TimePickerDialog.newInstance(
                CreateCommitmentActivity.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                true
        );
//        dpd.setMinTime(now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), 0);
//        dpd.setTimeInterval(1, 1, 1);
        dpd.dismissOnPause(true);
        dpd.show(getFragmentManager(),
                TIMEPICKER_DIALOG);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        System.out.println("view = [" + view + "], year = [" + year + "], monthOfYear = [" + monthOfYear + "], dayOfMonth = [" + dayOfMonth + "]");
        txtDate.setText(String.format(DATE_FORMAT, dayOfMonth, monthOfYear + 1, year));
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        System.out.println("view = [" + view + "], hourOfDay = [" + hourOfDay + "], minute = [" + minute + "], second = [" + second + "]");
        txtTime.setText(String.format(TIME_FORMAT, hourOfDay, minute));
    }

    @OnClick(R.id.txtRing)
    protected void openSelectRing() {
        Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Selecione o toque");
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, (Uri) null);
        this.startActivityForResult(intent, REQUEST_CODE_RINGTONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_RINGTONE && resultCode == Activity.RESULT_OK) {
            uriRingTone = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
            if (uriRingTone != null) {
                Ringtone ringtone = RingtoneManager.getRingtone(this, uriRingTone);
                ringtoneTitle = ringtone.getTitle(this);
                ringtone.stop();
            }
            txtRing.setText(ringtoneTitle);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}

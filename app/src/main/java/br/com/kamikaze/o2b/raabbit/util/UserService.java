package br.com.kamikaze.o2b.raabbit.util;

import com.google.gson.JsonObject;

import br.com.kamikaze.o2b.raabbit.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Felipe Novaes F. da Rocha (Kamikaze) on 11/06/16.
 */
public interface UserService {

    @POST("users")
    Call<User> addUser(@Body User body);
    @POST("users/login")
    Call<JsonObject> login(@Body User body);
}

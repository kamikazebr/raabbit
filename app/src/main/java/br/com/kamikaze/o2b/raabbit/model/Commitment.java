package br.com.kamikaze.o2b.raabbit.model;

import android.net.Uri;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.Emitter;
import com.couchbase.lite.Mapper;
import com.couchbase.lite.Query;
import com.couchbase.lite.UnsavedRevision;
import com.couchbase.lite.View;
import com.couchbase.lite.util.Log;
import com.evernote.android.job.JobManager;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import br.com.kamikaze.o2b.raabbit.RaabbitApplication;
import br.com.kamikaze.o2b.raabbit.RaabbitJob;

/**
 * Created by Felipe Novaes F. da Rocha (Kamikaze) on 09/06/16.
 */
public class Commitment extends BaseModel {
    public static final String DD_MM_YYYY = "dd/MM/yyyy";
    public static final String HH_MM = "HH:mm";
    public static final String DD_MM_YYYY_HH_MM = "dd/MM/yyyy HH:mm";

    private static final PrettyTime pt = new PrettyTime(new Locale("pt"));
    private static final String DOC_TYPE = "commitment";
    private static final String VIEW_NAME = "commitments";
    public static final DateTimeFormatter formatter = DateTimeFormat.forPattern(DD_MM_YYYY_HH_MM);
    private String _id;
    private String ringtone;
    private String ringtoneTitle;

//    @Expose(serialize = false, deserialize = false)
//    private boolean enabled;

    public void setRingtone(String ringtone) {
        this.ringtone = ringtone;
    }

    public String getRingtone() {
        return ringtone;
    }

    public String getRingtoneTitle() {
        return ringtoneTitle;
    }

    public void setRingtoneTitle(String ringtoneTitle) {
        this.ringtoneTitle = ringtoneTitle;
    }

    public Uri getRingtoneUri() {
        if (getRingtone() != null) {
            return Uri.parse(getRingtone());
        }
        return null;
    }
//
//    public boolean isEnabled() {
//        return enabled;
//    }
//
//    public void setEnabled(boolean enabled) {
//        this.enabled = enabled;
//    }

    public enum Reminder {
        MIN_15(15), MIN_20(20), MIN_25(25), MIN_30(15), MIN_45(45), HOUR_1(1 * 60), HOUR_2(2 * 60);
        private int minutes;

        Reminder(int minutes) {
            this.minutes = minutes;
        }

        public int getMinutes() {
            return minutes;
        }
    }


    String title;
    String description;
    DateTime dateLimit;
    Reminder reminder;
    String type = DOC_TYPE;
    List<String> channels = new ArrayList<String>();

    public Commitment() {
    }

    public Commitment(Document item) {
        _id = (String) item.getProperty("_id");
        String title = (String) item.getProperty("title");
        String dateString = (String) item.getProperty("dateLimit");
        ringtone = (String) item.getProperty("ringtone");
        ringtoneTitle = (String) item.getProperty("ringtoneTitle");
        DateTime date = null;
        if (dateString != null) {
            date = formatter.parseDateTime(dateString);
        }

        setDescription((String) item.getProperty("description"));
        setTitle(title);
        setDateLimit(date);
    }


    public Commitment(String title, String description, DateTime dateLimit) {
        this.title = title;
        this.description = description;
        this.dateLimit = dateLimit;
    }

    public String getId() {
        return _id;
    }

    public void setId(String id) {
        this._id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DateTime getDateLimit() {
        return dateLimit;
    }

    public boolean isPast() {
        long millis = getDateLimit().getMillis();
        long currentTimeMillis = System.currentTimeMillis();
        return millis <= currentTimeMillis;
    }

    public void setDateLimit(DateTime dateLimit) {
        this.dateLimit = dateLimit;
    }

    public static String getDatePretty(DateTime date) {
        String pretty = null;
        if (date != null) {
            pretty = pt.format(date.toDate());
        }
        return pretty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Commitment that = (Commitment) o;

        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null)
            return false;
        if (dateLimit != null ? !dateLimit.equals(that.dateLimit) : that.dateLimit != null)
            return false;
        return reminder == that.reminder;

    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (dateLimit != null ? dateLimit.hashCode() : 0);
        result = 31 * result + (reminder != null ? reminder.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Commitment{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", dateLimit=" + dateLimit +
                '}';
    }

    public static Query getQuery(Database database) {
        View view = database.getView(VIEW_NAME);
        if (view.getMap() == null) {
            Mapper mapper = new Mapper() {
                public void map(Map<String, Object> document, Emitter emitter) {
                    String type = (String) document.get("type");
                    if (DOC_TYPE.equals(type)) {
                        if (document.containsKey("dateLimit")) {
                            emitter.emit(document.get("dateLimit"), document);
                        } else {
                            emitter.emit(document.get("_id"), document);
                        }

//                        emitter.emit(document.get("title"), document);
                    }
                }
            };
            view.setMap(mapper, "3");
        }

        Query query = view.createQuery();
        return query;
    }

    public Document createOrUpdateCommitment(RaabbitApplication app, String channel) throws CouchbaseLiteException {

        Database database = app.getDatabase();
        Database mainDatabase = app.getMainDatabase();

        Map<String, Object> properties = toJSONMap();


        if (dateLimit != null) {
            properties.put("dateLimit", dateLimit.toString(DD_MM_YYYY_HH_MM));
        }
        channels = new ArrayList<String>();
        channels.add(channel);
        properties.put("channels", channels);
        Log.d(RaabbitApplication.TAG, properties.toString());
        Document document = null;
        if (_id != null) {
            document = database.getExistingDocument(_id);
        } else {
            document = database.createDocument();
        }

        UnsavedRevision revision = document.createRevision();
        revision.setUserProperties(properties);

        revision.save();


        Map<String, Object> prop = new HashMap<String, Object>();
        prop.put("enabled", true);
        try {
            mainDatabase.putLocalDocument(document.getId(), prop);
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }

        Log.d(RaabbitApplication.TAG, "Created doc: %s", properties);

        return document;
    }

    public void updateEnableStatus(RaabbitApplication application, boolean isChecked) {
//        Map<String, Object> prop = new HashMap<String, Object>();
        Map<String, Object> prop = application.getMainDatabase().getExistingLocalDocument(getId());
        if (isChecked) {
            Object jobID = prop.get("job");
            if (jobID == null || ((Integer) jobID) == -1) {
                String docId = getId();
                String title = getTitle();
                long millis = getDateLimit().getMillis();
                int job = RaabbitJob.scheduleJob(docId, title, millis, getRingtoneUri());
                prop.put("job", job);
            }
        } else {
            if (prop != null) {
                Object job = prop.get("job");
                if (job != null) {
                    boolean cancelled = JobManager.instance().cancel((Integer) job);
                    Log.d(RaabbitApplication.TAG, "Job Cancelled:" + cancelled);
                }
            }
        }
        prop.put("enabled", isChecked);
        try {
            application.getMainDatabase().putLocalDocument(getId(), prop);
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
    }
}

package br.com.kamikaze.o2b.raabbit;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

/**
 * Created by Felipe Novaes F. da Rocha (Kamikaze) on 15/06/16.
 */
public class RaabbitJobCreator implements JobCreator {
    @Override
    public Job create(String tag) {
        switch (tag) {
            case RaabbitJob.TAG:
                return new RaabbitJob();
        }
        return null;
    }
}

package br.com.kamikaze.o2b.raabbit.util;

import java.io.IOException;

import br.com.kamikaze.o2b.raabbit.RaabbitApplication;
import br.com.kamikaze.o2b.raabbit.model.User;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Felipe Novaes F. da Rocha (Kamikaze) on 11/06/16.
 */
public class RestManager {
    private static final RestManager instance = new RestManager();
    private static final String BASE_URL = "http://sig.sigmaseis.com.br:3000";
//    private static final String BASE_URL = "http://192.168.56.1:3000";
//    private static final String BASE_URL = "http://localhost:3000";
    private final UserService userService;

    private RestManager() {
        Retrofit build = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(GsonUtil.getInstance().getGson()))
                .build();
        userService = build.create(UserService.class);

    }

    public static RestManager getInstance() {
        return instance;
    }

    public UserService getUserService() {
        return userService;
    }
    public Response<User> testUserService() {
        User user = new User("felipe","windholyghost@gmail.com", RaabbitApplication.toMD5("123"));

        Call<User> userCall = userService.addUser(user);
        Response<User> execute = null;
        try {
            execute = userCall.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return execute;
    }
}

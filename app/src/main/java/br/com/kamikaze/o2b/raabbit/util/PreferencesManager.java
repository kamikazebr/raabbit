package br.com.kamikaze.o2b.raabbit.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Felipe Novaes F. da Rocha (Kamikaze) on 09/06/16.
 */
public class PreferencesManager {

    private static final String PREF_CURRENT_USER_ID = "PREF_CURRENT_USER_ID";
    private static final String PREF_CURRENT_USER_EMAIL = "PREF_CURRENT_USER_EMAIL";

    private static final String PREF_LOGGED = "PREF_LOGGED";
    private static final String PREF_FB_ACCESS_TOKEN = "PREF_FB_ACCESS_TOKEN";
    private final SharedPreferences raabbit;


    public PreferencesManager(Context context){
         raabbit = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean isLogged() {
        return raabbit.getBoolean(PREF_LOGGED,false);
    }

    public void setLogged(boolean logged) {
        raabbit.edit().putBoolean(PREF_LOGGED, logged).apply();
    }

    public String getCurrentUserIDOrEmail() {
        if (getCurrentUserId()==null){
            return getCurrentUserEmail();
        }
        return getCurrentUserId();
    }

    public String getCurrentUserEmail() {
        return raabbit.getString(PREF_CURRENT_USER_EMAIL,null);
    }
    public void setCurrentUserEmail(String email) {
        raabbit.edit().putString(PREF_CURRENT_USER_EMAIL,email).apply();
    }
    public String getCurrentUserId() {
        return raabbit.getString(PREF_CURRENT_USER_ID,null);
    }

    public void setCurrentUserId(String userId) {
        raabbit.edit().putString(PREF_CURRENT_USER_ID,userId).apply();
    }

    public void setFbAccessToken(String token) {
        raabbit.edit().putString(PREF_FB_ACCESS_TOKEN,token).apply();
    }

    public String getFbAccessToken() {
        return raabbit.getString(PREF_FB_ACCESS_TOKEN,null);
    }
}

package br.com.kamikaze.o2b.raabbit.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.couchbase.lite.CouchbaseLiteException;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import br.com.kamikaze.o2b.raabbit.R;
import br.com.kamikaze.o2b.raabbit.RaabbitApplication;
import br.com.kamikaze.o2b.raabbit.model.User;
import br.com.kamikaze.o2b.raabbit.util.PreferencesManager;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginChooserActivity extends AppCompatActivity {
    public static final String FACEBOOK_PERMISSIONS_EMAIL = "email";
    private LoginButton loginFaceButton;
    private CallbackManager callbackManager;
    private PreferencesManager preferences;
    private RaabbitApplication application;
    private Button loginButton;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_chooser);
        ButterKnife.inject(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        application = (RaabbitApplication) getApplication();
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);

        progressDialog.setMessage("Autenticando...");
        preferences = new PreferencesManager(application);
        loginButton = (Button) findViewById(R.id.btn_login);
        loginFaceButton = (LoginButton) findViewById(R.id.btn_face);
        loginFaceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
            }
        });
        loginFaceButton.setReadPermissions(Arrays.asList(FACEBOOK_PERMISSIONS_EMAIL));
        callbackManager = CallbackManager.Factory.create();
        loginFaceButton.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        loginUser(loginResult);
                    }

                    @Override
                    public void onCancel() {
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.e(LoginChooserActivity.class.getName(),exception.toString());
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                    }
                });
    }

    private void loginUser(final LoginResult loginResult) {
        System.out.println("LoginChooserActivity.loginUser");
        if (preferences.getCurrentUserId() == null) {
            GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                            System.out.println("LoginChooserActivity.onCompleted");
                            if (preferences.getCurrentUserId() == null) {
                                String userId = null;
                                String name = null;
                                String email = null;
                                try {
                                    userId = jsonObject.getString("id");
                                    name = jsonObject.getString("name");
                                    email = jsonObject.optString("email", null);
                                    Log.i(LoginChooserActivity.class.getSimpleName(),userId+":"+name);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                User user = new User();
                                user.setEmail(email);
                                user.setUserID(userId);
                                user.setName(name);
                                try {
                                    user.createOrUpdate(application.getMainDatabase());
                                } catch (CouchbaseLiteException e) {
                                    e.printStackTrace();
                                }

//                                Toast.makeText(LoginChooserActivity.this, "Login successful!  Starting sync.", Toast.LENGTH_LONG).show();

                                preferences.setCurrentUserId(userId);
                                preferences.setCurrentUserEmail(email);
                                preferences.setFbAccessToken(loginResult.getAccessToken().getToken());
                                preferences.setLogged(true);
                                if (progressDialog != null) {
                                    progressDialog.dismiss();
                                }
                                Intent i = new Intent(LoginChooserActivity.this, MainActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                finish();
                            } else {
                                if (progressDialog != null) {
                                    progressDialog.dismiss();
                                }
                                preferences.setLogged(true);
                                Intent i = new Intent(LoginChooserActivity.this, MainActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                finish();
                            }

                        }
                    }).executeAsync();
        }
    }



    @OnClick(R.id.btn_login)
    protected void loginButtonClick(View bt){
        //TODO Redirect to login activity
        startActivity(new Intent(LoginChooserActivity.this,LoginActivity.class));
    }

    @OnClick(R.id.btn_create_user)
    protected void createUserButtonClick(View v){
        startActivity(new Intent(LoginChooserActivity.this,CreateUserActivity.class));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}

package br.com.kamikaze.o2b.raabbit.activities;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import br.com.kamikaze.o2b.raabbit.R;
import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Felipe Novaes F. da Rocha (Kamikaze) on 11/06/16.
 */
public abstract class BaseToolbarActivity extends AppCompatActivity {



    @InjectView(R.id.toolbar)
    protected Toolbar toolbar;


    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.inject(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}

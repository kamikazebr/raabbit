package br.com.kamikaze.o2b.raabbit.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
@Deprecated
/**
 * Created by Felipe Novaes F. da Rocha (Kamikaze) on 10/06/16.
 */
public class DatabaseStub {

    List<Commitment> items = new ArrayList<>();

    private static DatabaseStub instance = new DatabaseStub();

    public static DatabaseStub getInstance(){
        return instance;
    }

    private DatabaseStub() {



//        items.add(new Commitment("Fazer o layout","", DateTime.now().plusDays(1)));
//        items.add(new Commitment("Terminar o app","", DateTime.now().plusDays(2).toDate()));
//        items.add(new Commitment("Entregar","", DateTime.now().plusMinutes(10).toDate()));
//        items.add(new Commitment("Tomar café","", DateTime.now().plusMinutes(32).toDate()));
//        items.add(new Commitment("Refatorar codigo","", DateTime.now().plusDays(15).toDate()));
//        items.add(new Commitment("Dormir","", DateTime.now().plusDays(100).toDate()));
//        items.add(new Commitment("Jogar xadrez","", DateTime.now().plusHours(1).toDate()));
//        items.add(new Commitment("Encontrar Aline","", DateTime.now().plusHours(3).toDate()));
//        items.add(new Commitment("Comprar presente","", DateTime.now().minusMinutes(16).minusHours(2).toDate()));
    }

    public List<Commitment> getItems() {
        sortItems();
        return items;
    }

    public void remove(Commitment commitment) {
        items.remove(commitment);
    }
    public void sortItems(){
        Collections.sort(items, new Comparator<Commitment>() {
            @Override
            public int compare(Commitment lhs, Commitment rhs) {
                return lhs.getDateLimit().compareTo(rhs.getDateLimit());
            }
        });
    }
//    public void setItems(List<Commitment> items) {
//        this.items = items;
//    }
}

package br.com.kamikaze.o2b.raabbit.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;

import com.andreabaccega.formedittextvalidator.Validator;
import com.andreabaccega.widget.FormEditText;

import br.com.kamikaze.o2b.raabbit.R;
import br.com.kamikaze.o2b.raabbit.fragments.InfoDialogFragment;
import br.com.kamikaze.o2b.raabbit.model.User;
import br.com.kamikaze.o2b.raabbit.util.PreferencesManager;
import br.com.kamikaze.o2b.raabbit.util.RestManager;
import butterknife.InjectView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateUserActivity extends BaseToolbarActivity {

    @InjectView(R.id.txtName)
    FormEditText txtName;

    @InjectView(R.id.txtEmail)
    FormEditText txtEmail;

    @InjectView(R.id.txtPassword)
    FormEditText txtPassword;

    @InjectView(R.id.txtPasswordConfirm)
    FormEditText txtPasswordConfirm;

    private PreferencesManager preferences;
    private AlertDialog alertDialog;
    private ProgressDialog autenticandoDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);

        txtPasswordConfirm.addValidator(new Validator("Senhas não são iguais") {
            @Override
            public boolean isValid(EditText et) {
                return TextUtils.equals(et.getText(), txtPassword.getText());
            }
        });

        preferences =  new PreferencesManager(this);

        alertDialog = new AlertDialog.Builder(this)
                .setTitle("Usuario já existe")
                .setMessage("Deseja ir para login?")
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        goToLogin();
                    }
                })
                .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();

    }

    @OnClick(R.id.btn_create)
    protected void onClickButtonCreate(){
        if (txtEmail.testValidity() && txtPassword.testValidity() && txtPasswordConfirm.testValidity() && txtName.testValidity()){
            User user = new User();
            final String email = txtEmail.getText().toString();
            user.setName(txtName.getText().toString());
            user.setEmail(email);
            user.setPassword(txtPassword.getText().toString());
            autenticandoDialog = ProgressDialog.show(this, "Autenticando", "Aguarde...", true, true);
            Call<User> userCall = RestManager.getInstance().getUserService().addUser(user);
            userCall.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    closeProgressDialog();
                    if (response.code() == 201) {
                        goToLogin();
                    }else if (response.code() == 409){
                        showDialog();
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    closeProgressDialog();
                    showErrorMessage(t);
                }
            });
        }
    }

    private void showErrorMessage(Throwable t) {
        final InfoDialogFragment infoDialogFragment = new InfoDialogFragment();
        infoDialogFragment.setMessage("Erro:" + t.getMessage());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                infoDialogFragment.show(getSupportFragmentManager(),"infoDialog");
            }
        });
    }

    private void showDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (alertDialog != null) {
                    alertDialog.show();
                }
            }
        });
    }

    private void closeProgressDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (autenticandoDialog != null) {
                    autenticandoDialog.dismiss();
                }
            }
        });
    }

    private void goToLogin() {

        //TODO Never back to here
        Intent intent = new Intent(CreateUserActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

}

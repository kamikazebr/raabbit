package br.com.kamikaze.o2b.raabbit.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.andreabaccega.widget.FormEditText;
import com.couchbase.lite.CouchbaseLiteException;
import com.google.gson.JsonObject;

import java.util.Observable;
import java.util.Observer;

import br.com.kamikaze.o2b.raabbit.R;
import br.com.kamikaze.o2b.raabbit.RaabbitApplication;
import br.com.kamikaze.o2b.raabbit.fragments.InfoDialogFragment;
import br.com.kamikaze.o2b.raabbit.model.User;
import br.com.kamikaze.o2b.raabbit.util.PreferencesManager;
import br.com.kamikaze.o2b.raabbit.util.RestManager;
import butterknife.InjectView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseToolbarActivity implements Observer {

    private PreferencesManager preferences;


    @InjectView(R.id.txt_email)
    FormEditText txtEmail;
    @InjectView(R.id.txt_password)
    FormEditText txtPassword;


    private String email;
    private String password;

    final InfoDialogFragment infoDialogFragment = new InfoDialogFragment();
    private RaabbitApplication app;
    private ProgressDialog autenticandoDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        preferences = new PreferencesManager(this);

        app = (RaabbitApplication) getApplication();




    }

    @OnClick(R.id.btn_entrar)
    protected void buttonEnterClick() {
        if (txtEmail.testValidity() && txtPassword.testValidity()) {

            email = txtEmail.getText().toString();
            password = txtPassword.getText().toString();

            User user = new User();
            user.setEmail(email);
            user.setPassword(password);
            final Call<JsonObject> login = RestManager.getInstance().getUserService().login(user);
            autenticandoDialog = ProgressDialog.show(this, "Autenticando", "Aguarde...", true, true);
            autenticandoDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    System.out.println("LoginActivity.onCancel");
                    login.cancel();
                }
            });
            login.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    switch (response.code()) {
                        case 200:
                            app.setDatabaseForName(email);
                            app.startReplicationSyncWithBasicAuth(txtEmail.getText().toString(), txtPassword.getText().toString());
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    closeProgressDialog();
                                    if (email != null) {
                                        saveOnPreferences();
                                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        LoginActivity.this.startActivity(intent);
                                        LoginActivity.this.finish();
                                    }
                                }
                            });
                            break;
                        case 401:
                            showMessage401();
                            break;
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.e(LoginActivity.class.getSimpleName(), "call = [" + call + "], t = [" + t + "]");
                    showMessage(t.getMessage());
                }
            });

        }
    }

    private void closeProgressDialog() {
        if (autenticandoDialog != null) {
            autenticandoDialog.dismiss();
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        showMessage401();
        app.getOnSyncUnauthorizedObservable().deleteObserver(this);
    }

    private void showMessage401() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                closeProgressDialog();
                if (!infoDialogFragment.isVisible()) {
                    infoDialogFragment.setMessage("Não autorizado!");
                    infoDialogFragment.show(getSupportFragmentManager(), "info");
                }
            }
        });
    }


    private void showMessage(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                closeProgressDialog();
                if (!infoDialogFragment.isVisible()) {
                    infoDialogFragment.setMessage(msg);
                    infoDialogFragment.show(getSupportFragmentManager(), "info");
                }
            }
        });
    }

    private void saveOnPreferences() {
        if (email != null && password != null) {
            preferences.setCurrentUserId(null);
            preferences.setCurrentUserEmail(email);
            preferences.setLogged(true);
            User user = new User();
            user.setEmail(email);
            user.setPassword(password);
            try {
                user.createOrUpdate(app.getMainDatabase());
            } catch (CouchbaseLiteException e) {
                e.printStackTrace();
            }
        }
    }

}

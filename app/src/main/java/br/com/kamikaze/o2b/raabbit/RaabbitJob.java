package br.com.kamikaze.o2b.raabbit;

import android.app.Notification;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;
import com.evernote.android.job.util.support.PersistableBundleCompat;

import br.com.kamikaze.o2b.raabbit.util.Notifications;

/**
 * Created by Felipe Novaes F. da Rocha (Kamikaze) on 15/06/16.
 */
public class RaabbitJob extends Job {

    public static final String TAG = "job_tag";
    @NonNull
    @Override
    protected Result onRunJob(Params params) {
        RaabbitApplication app = (RaabbitApplication) getContext().getApplicationContext();
        //TODO Remove Job from CouchLiteBase
        PersistableBundleCompat extras = params.getExtras();
        String title = extras.getString("title", "");
        String uriString = extras.getString("ringtone", null);
        Uri uri = null;
        if (uriString != null) {
            uri = Uri.parse(uriString);
        }
        Log.d(RaabbitJob.class.getSimpleName(), Thread.currentThread().toString());
        Notification notification = Notifications.getInstance().getNotification(getContext(), "Compromisso", title,uri);
        Notifications.getInstance().notity(getContext(),notification,1);
        return Result.SUCCESS;
    }


    public static int scheduleJob(String docId, String title, long millis, Uri ringtone) {
        PersistableBundleCompat extras = new PersistableBundleCompat();
        extras.putString("title", title);
        extras.putString("docId",docId);
        if (ringtone!=null) {
            extras.putString("ringtone", ringtone.toString());
        }
        extras.putLong("millis", millis);
        millis = getMillis(millis);
        if (millis >0) {
            return new JobRequest.Builder(RaabbitJob.TAG)
                    .setExact(millis)
                    .setExtras(extras)
                    .setPersisted(true)
                    .build()
                    .schedule();
        }
        return -1;
    }

    public static long getMillis(long millis) {
        if (millis == -1) {
            millis = 1_000L;
        }else{
            millis = millis - System.currentTimeMillis();
        }
        return millis;
    }


}

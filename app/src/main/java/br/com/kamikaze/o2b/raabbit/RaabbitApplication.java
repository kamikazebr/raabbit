package br.com.kamikaze.o2b.raabbit;

import android.app.Application;
import android.content.Context;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Manager;
import com.couchbase.lite.android.AndroidContext;
import com.couchbase.lite.replicator.Replication;
import com.couchbase.lite.util.Log;
import com.evernote.android.job.JobManager;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

import net.danlew.android.joda.JodaTimeAndroid;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import br.com.kamikaze.o2b.raabbit.util.PreferencesManager;
import br.com.kamikaze.o2b.raabbit.util.Synchronize;

/**
 * Created by developer on 09/06/16.
 */
public class RaabbitApplication extends Application {


    public static final String TAG = "RaabbitApplication";
    private static final String DATABASE_NAME = "raabbit";
    private static final String SYNC_URL_HTTP = BuildConfig.SYNC_URL_HTTP;
    private static final String SYNC_URL_HTTPS = BuildConfig.SYNC_URL_HTTPS;
    private static final String SYNC_URL = SYNC_URL_HTTP;

    private Manager manager;
    private Database database;
    private Database usersDatabase;

    private Synchronize sync;
    private PreferencesManager preferences;
    private OnSyncProgressChangeObservable onSyncProgressChangeObservable;
    private OnSyncUnauthorizedObservable onSyncUnauthorizedObservable;
    private OnLogonObservable onLogonObservable;

    @Override
    public void onCreate() {
        super.onCreate();

        preferences = new PreferencesManager(getApplicationContext());
        JobManager.create(this).addJobCreator(new RaabbitJobCreator());
//        JobManager.instance().cancelAll();
        FacebookSdk.sdkInitialize(this);
        JodaTimeAndroid.init(this);
        initObservable();
        initDatabase();
    }

    public void logoutUser() {
        callFacebookLogout(getApplicationContext());

        preferences.setCurrentUserId(null);
        preferences.setCurrentUserEmail(null);
        preferences.setFbAccessToken(null);
        preferences.setLogged(false);
        if (sync != null) {
            sync.destroyReplications();
            sync = null;
        }
//        preferences.setCurrentListId(null);
    }

    /**
     * Logout from Facebook to make sure the session and cache are cleared
     * See http://stackoverflow.com/a/18584885/1908348
     */
    public static void callFacebookLogout(Context context) {
        LoginManager.getInstance().logOut();
    }

    private void initDatabase() {
        try {

            Manager.enableLogging(TAG, Log.VERBOSE);
            Manager.enableLogging(Log.TAG, Log.VERBOSE);
            Manager.enableLogging(Log.TAG_SYNC_ASYNC_TASK, Log.VERBOSE);
            Manager.enableLogging(Log.TAG_SYNC, Log.VERBOSE);
            Manager.enableLogging(Log.TAG_QUERY, Log.VERBOSE);
            Manager.enableLogging(Log.TAG_VIEW, Log.VERBOSE);
            Manager.enableLogging(Log.TAG_DATABASE, Log.VERBOSE);

            manager = new Manager(new AndroidContext(getApplicationContext()), Manager.DEFAULT_OPTIONS);
        } catch (IOException e) {
            Log.e(TAG, "Cannot create Manager object", e);
            return;
        }

        try {
            usersDatabase = manager.getDatabase(DATABASE_NAME);

        } catch (CouchbaseLiteException e) {
            Log.e(TAG, "Cannot get Database", e);
            return;
        }
    }


    public void startReplicationSyncWithBasicAuth(String username, String password) {
        if (sync != null) {
            sync.destroyReplications();
        }
        sync = new Synchronize.Builder(getDatabase(), SYNC_URL, true)
                .basicAuth(username, password)
                .addChangeListener(getReplicationChangeListener())
                .build();
        sync.start();

    }

    public void startReplicationSyncWithCustomCookie(String cookieValue) {

        sync = new Synchronize.Builder(getDatabase(), SYNC_URL, true)
                .cookieAuth(cookieValue)
                .addChangeListener(getReplicationChangeListener())
                .build();
        sync.start();

    }

    public void startReplicationSyncWithStoredCustomCookie() {

        sync = new Synchronize.Builder(getDatabase(), SYNC_URL, true)
                .addChangeListener(getReplicationChangeListener())
                .build();
        sync.start();

    }

    public void startReplicationSyncWithFacebookLogin(String accessToken) {

        if (sync != null) {
            sync.destroyReplications();
        }

        List<String> channels = new ArrayList<String>();
        channels.add(preferences.getCurrentUserId());
        sync = new Synchronize.Builder(getDatabase(), SYNC_URL, true)
                .facebookAuth(accessToken)
                .setChannels(channels)
                .addChangeListener(getReplicationChangeListener())
                .build();
        sync.start();

    }

    public void stopSync() {
        sync.destroyReplications();
        sync = null;
    }

    private Replication.ChangeListener getReplicationChangeListener() {
        return new Replication.ChangeListener() {

            @Override
            public void changed(Replication.ChangeEvent event) {
                Replication replication = event.getSource();
                if (event.getError() != null) {
                    Throwable lastError = event.getError();
                    String message = lastError.getMessage();
                    if (message.contains("existing change tracker")) {
                        Log.d("Replication Event", String.format("Sync error: %s:", lastError.getMessage()));
                    } else if (message.contains("Unauthorized")) {
                        onSyncUnauthorizedObservable.notifyChanges();
                    }
                    Log.d(TAG, lastError.toString());
//                    if (lastError instanceof HttpResponseException) {
//                        HttpResponseException responseException = (HttpResponseException) lastError;
//                        if (responseException.getStatusCode() == 401) {
//                            onSyncUnauthorizedObservable.notifyChanges();
//                        }
//                    }
                }
//                switch (replication.getStatus()) {
//                    case REPLICATION_ACTIVE:
//                    case REPLICATION_IDLE:
//                        onLogonObservable.notifyChanges();
//                        break;
//                }

                Log.d(TAG, replication.getStatus().toString());
                updateSyncProgress(
                        replication.getCompletedChangesCount(),
                        replication.getChangesCount(),
                        replication.getStatus()
                );
            }
        };
    }

    //TODO IMPROVEMENT - Maybe use Broadcasts
    private void initObservable() {
        onSyncProgressChangeObservable = new OnSyncProgressChangeObservable();
        onSyncUnauthorizedObservable = new OnSyncUnauthorizedObservable();
        onLogonObservable = new OnLogonObservable();
    }


    private synchronized void updateSyncProgress(int completedCount, int totalCount, Replication.ReplicationStatus status) {
        onSyncProgressChangeObservable.notifyChanges(completedCount, totalCount, status);
    }

    public OnSyncProgressChangeObservable getOnSyncProgressChangeObservable() {
        return onSyncProgressChangeObservable;
    }

    public OnSyncUnauthorizedObservable getOnSyncUnauthorizedObservable() {
        return onSyncUnauthorizedObservable;
    }
    @Deprecated
    public OnLogonObservable getOnLogonObservable() {
        return onLogonObservable;
    }

    public static class OnLogonObservable extends Observable {
        private void notifyChanges() {
            setChanged();
            notifyObservers();
        }
    }

    public static class OnSyncProgressChangeObservable extends Observable {
        private void notifyChanges(int completedCount, int totalCount, Replication.ReplicationStatus status) {
            SyncProgress progress = new SyncProgress();
            progress.completedCount = completedCount;
            progress.totalCount = totalCount;
            progress.status = status;
            setChanged();
            notifyObservers(progress);
        }
    }

    public static class OnSyncUnauthorizedObservable extends Observable {
        private void notifyChanges() {
            setChanged();
            notifyObservers();
        }
    }

    public static class SyncProgress {
        public int completedCount;
        public int totalCount;
        public Replication.ReplicationStatus status;
    }

    public Database getDatabase() {
        return this.database;
    }

    public Database getMainDatabase() {
        return this.usersDatabase;
    }

    public Database setDatabaseForName(String name) {
        try {
            database = manager.getDatabase("db" + toMD5(name));
        } catch (CouchbaseLiteException e) {
            Log.e(TAG, "Cannot get Database", e);
        }
        return database;
    }

    public static String toMD5(String plain) {
        if (plain != null) {
            MessageDigest digest = null;
            try {
                digest = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            byte[] inputBytes = plain.getBytes();
            byte[] hashBytes = digest.digest(inputBytes);
            return byteArrayToHex(hashBytes);
        }
        return null;
    }

    public static String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for (byte b : a)
            sb.append(String.format("%02x", b & 0xff));
        return sb.toString();
    }
}

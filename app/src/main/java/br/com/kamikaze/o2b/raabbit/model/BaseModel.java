package br.com.kamikaze.o2b.raabbit.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Map;

import br.com.kamikaze.o2b.raabbit.util.GsonUtil;

/**
 * Created by Felipe Novaes F. da Rocha (Kamikaze) on 11/06/16.
 */
public abstract class BaseModel implements Serializable {


    public String toJSON(){
        return GsonUtil.getInstance().getGson().toJson(this);
    }

    public Map<String,Object> toJSONMap(){
        Type type = new TypeToken<Map<String, Object>>() {
        }.getType();
        Gson gson = GsonUtil.getInstance().getGson();

        return gson.fromJson(gson.toJson(this), type);
    }

    @Override
    public String toString() {
        return toJSON();
    }
}
